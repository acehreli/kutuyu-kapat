/**
 * Oyundaki kutuları yönetir. MVC yönteminin 'Model' tarafı.
 */

module oyun;

import std.exception;
import std.algorithm;
import kutu_ilgilisi;
import kutu_durumu;

enum size_t kutuAdedi = 9;

class KutuOyunu
{
    KutuDurumu[kutuAdedi] kutular = KutuDurumu.açık;
    Kutuİlgilisi[] ilgililer;

    static size_t indeksi(size_t numara)
    {
        return numara - 1;
    }

    void ilgiliyiTanı(Kutuİlgilisi ilgili)
    {
        ilgililer ~= ilgili;
        ilgili.değişti(kutular);  /* İlgiliye ilk bilgiyi gönderelim. */
    }

    KutuDurumu durum(size_t numara) const
    {
        return kutular[indeksi(numara)];
    }

    void kapat(const size_t[] numaralar...)
    {
        foreach (numara; numaralar) {
            kutular[indeksi(numara)] = KutuDurumu.kapalı;
        }

        foreach (ilgili; ilgililer) {
            ilgili.değişti(kutular);
        }
    }

    bool bitti_mi() const @property
    {
        return !kutular[].canFind(KutuDurumu.açık);
    }

    size_t puan() const @property
    {
        return kutular[].count(KutuDurumu.kapalı);
    }
}
