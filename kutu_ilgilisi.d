/**
 * Kutuların durumuyla ilgilenen türlerin arayüzünü belirler. Observer
 * pattern'daki 'observer' arayüzüdür.
 */

module kutu_ilgilisi;

import kutu_durumu;

interface Kutuİlgilisi
{
    void değişti(const KutuDurumu[] kutular);
}
