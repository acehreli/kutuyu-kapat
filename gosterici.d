/**
 * Oyunun kullanıcıyla etkileşim arayüzünü belirler. MVC yönteminin 'View'
 * arayüzü.
 */

module gosterici;

import kutu_durumu;
import kutu_ilgilisi;

interface Gösterici : Kutuİlgilisi
{
    /* Kullanıcıya mesaj gösterir. */
    void mesajVer(string mesaj);

    /* Kullanıcıdan kutu numaralarını alır. */
    size_t[] numaralarOku();

    /* Kutuların durumunda değişiklik olduğunda oyun tarafından çağrılır. */
    void değişti(const KutuDurumu[] kutular);

    /* Zar atıldığında çağrılır. */
    void zarAtıldı(const size_t[] zarlar);
}
