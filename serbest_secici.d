/**
 * Numaraları denetlemeden seçer. Hata ayıklama sırasında yararlıdır.
 */

module serbest_secici;

import secici;
import gosterici;

class SerbestSeçici : Seçici
{
    Gösterici gösterici;

    this(Gösterici gösterici)
    {
        this.gösterici = gösterici;
    }

    size_t[] seç()
    {
        return gösterici.numaralarOku();
    }
}
