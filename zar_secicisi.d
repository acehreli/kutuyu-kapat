/**
 * Numaraları atılan zar değerlerine eşit olacak biçimde seçer. "Shut the box"
 * oyununun kurallarına uygun olan seçicidir.
 */

module zar_secicisi;

import std.range;
import std.algorithm;
import std.random;
import std.string;
import secici;
import gosterici;

class ZarSeçicisi : Seçici
{
    Gösterici gösterici;

    this(Gösterici gösterici)
    {
        this.gösterici = gösterici;
    }

    private static
    size_t[] zarAt()
    {
        return iota(2).map!(a => uniform(cast(size_t)1, 7)).array;
    }

    size_t[] seç()
    {
        const zarlar = zarAt();
        immutable zarToplamı = reduce!((a, b) => a + b)(cast(size_t)0, zarlar);

        gösterici.zarAtıldı(zarlar);

        size_t[] seçilenler;
        bool yasal_mı = false;

        while (!yasal_mı) {
            seçilenler = gösterici.numaralarOku();

            if (seçilenler.empty) {
                // Oyuncu çıkmak istemiş
                yasal_mı = true;

            } else {
                immutable seçimToplamı = reduce!((a, b) => a + b)
                                         (cast(size_t)0, seçilenler);

                if (seçimToplamı == zarToplamı) {
                    yasal_mı = true;

                } else {
                    gösterici.mesajVer(
                        format("Toplam %s yasal değil. ", seçimToplamı));
                    yasal_mı = false;
                }
            }
        }

        return seçilenler;
    }
}
