/**
 * Oyunda rol alan parçaları oluşturur ve oyunu başlatır.
 */

module main;

import oyun;
import konsol_gosterici;
import oynatici;

version (serbest_secim) {
    import serbest_secici;
    alias SerbestSeçici EtkinSeçiciTürü;

} else {
    import zar_secicisi;
    alias ZarSeçicisi EtkinSeçiciTürü;
}

void main()
{
    auto gösterici = new KonsolGösterici();

    auto oyun = new KutuOyunu();
    oyun.ilgiliyiTanı(gösterici);

    auto seçici = new EtkinSeçiciTürü(gösterici);

    auto oynatıcı = new Oynatıcı(oyun, seçici, gösterici);
    oynatıcı.oynat();
}
