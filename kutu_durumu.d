/**
 * Kutuların bulunabilecekleri durumları belirler.
 */

module kutu_durumu;

enum KutuDurumu { açık, kapalı }
