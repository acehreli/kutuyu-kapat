/**
 * Oyunu yönetir. MVC programlama yönteminin 'Controller' tarafı.
 */

module oynatici;

import std.array;
import std.string;
import oyun;
import kutu_durumu;
import secici;
import gosterici;

class Oynatıcı
{
    KutuOyunu oyun;
    Seçici seçici;
    Gösterici gösterici;

    this(KutuOyunu oyun, Seçici seçici, Gösterici gösterici)
    {
        this.oyun = oyun;
        this.seçici = seçici;
        this.gösterici = gösterici;
    }

    void oynat()
    {
        bool çıkılsın_mı = false;

        while (!çıkılsın_mı && !oyun.bitti_mi()) {
            const numaralar = seçici.seç();

            if (numaralar.empty) {
                // Oyuncu çıkmak istemiş
                çıkılsın_mı = true;
            }

            bool yasal_mı = true;

            foreach (size_t numara; numaralar) {
                if ((numara < 1) || (numara >  kutuAdedi)) {
                    gösterici.mesajVer(
                        format("%s geçerli bir numara değil.\n", numara));
                    yasal_mı = false;

                } else if (oyun.durum(numara) == KutuDurumu.kapalı) {
                    gösterici.mesajVer(
                        format("%s zaten kapalı.\n", numara));
                    yasal_mı = false;
                    çıkılsın_mı = true;
                }
            }

            if (yasal_mı && !çıkılsın_mı) {
                oyun.kapat(numaralar);
            }
        }

        immutable puan = oyun.puan;
        gösterici.mesajVer(
            format("Puan: %s%s\n", puan, puan == kutuAdedi ? "! :)" : ""));
    }
}
