all: kutuyu-kapat

DMD := dmd

KAYNAKLAR := \
gosterici.d \
konsol_gosterici.d \
kutu_durumu.d \
kutu_ilgilisi.d \
main.d \
oynatici.d \
oyun.d \
secici.d \
serbest_secici.d \
zar_secicisi.d \

SECENEKLER := \
${EK_DMD_SECENEKLERI} \
-unittest \
-w \

#-version=serbest_secim \

DOSYALAR := \
${KAYNAKLAR} \
Makefile \

kutuyu-kapat: ${DOSYALAR}
	${DMD} ${KAYNAKLAR} -of$@ ${SECENEKLER}
