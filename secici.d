/**
 * Numara seçim arayüzünü belirler.
 */

module secici;

interface Seçici
{
    size_t[] seç();
}
